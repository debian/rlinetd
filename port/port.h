#ifndef H_PORT
#define H_PORT

#include <stdarg.h>
#include <config.h>

#ifndef HAVE_STRDUP
char *strdup(const char *);
#endif

#ifndef HAVE_SNPRINTF
int snprintf(char *, size_t, const char *, ...);
#endif

#ifndef HAVE_VSNPRINTF
int vsnprintf(char *, size_t, const char *, va_list);
#endif

#ifdef HAVE_STAT_EMPTY_STRING_BUG
# define stat rpl_stat
#endif

#ifdef HAVE_LSTAT_EMPTY_STRING_BUG
# define lstat rpl_lstat
#endif

#endif /* !H_PORT */
