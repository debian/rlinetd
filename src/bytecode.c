#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifdef HAVE_CAPABILITIES
#include <sys/capability.h>
#endif

#ifdef HAVE_TCP_WRAPPERS
#include <tcpd.h>
#endif

#include "buffer.h"
#include "bytecode.h"
#include "db.h"
#include "error.h"
#include "rlinetd.h"
#include "signals.h"
#include "stack.h"
#ifdef BYTECODE_DEBUG
#include "assemble.h"
#endif

/* we avoid the more tedious sanity checks by assuming that since
	 we wrote the opcodes, they're going to be what we accept. dodgy. */

#define rl_fail1(a) \
	rl_warn("%s(%d) failed %s", (a), *op, strerror(errno))

#define RL_MIN(a, b) ((a) < (b) ? (a) : (b))

int allow_severity = LOG_INFO;
int deny_severity = LOG_WARNING;

#ifdef BYTECODE_DEBUG
static char * bytecode2string(rl_opcode_t op);
static char * bytecodeargs2string(rl_opcode_t* op);
#endif

void run_bytecode(rl_opcode_t *op, struct rl_instance *inst) {
	if(!op)
		return;
	if(rl_debug > 1)
		fprintf(stderr, "sched(%d)\n", getpid());
	do {
		if(rl_debug > 1)
#ifdef BYTECODE_DEBUG
			fprintf(stderr, "%d: op %d: %s (%s)\n", getpid(), *op, bytecode2string(*op), bytecodeargs2string(op));
#else
			fprintf(stderr, "op %d\n", *op);
#endif
		switch(*op) {
			struct argvtab *argv;
			struct logtab *log;
			int i, j, k;
#ifdef HAVE_TCP_WRAPPERS
			struct request_info *request;
#endif
			char *str;
			gid_t gid;
			struct semaphore *sem;
			struct buftab *bt;

			case OP_EXEC:
				closelog();
				fcntl(inst->sock, F_SETFD, 0);
				fcntl(inst->sock, F_SETFL, 0);
				j = *++op;
				argv = argvtab_get(*++op);
				argv_build(inst, argv);
				str = (j != -1) ? stringtab_get(j) : argv->argv[0];
				pid_t ssid = -1;
				while ((ssid = setsid() < 0) && errno == EAGAIN)
					;
				if (ssid < 0)
				{
					rl_warn("setsid()) for %s: %s", str, strerror(errno));
					exit(EX_DATAERR);
				}
				for(i = 0; i < 3; i++)
					if(inst->sock != i)
						dup2(inst->sock, i);
				if(inst->sock >= 3)
					close(inst->sock);
				if(execv(str, argv->argv)) {
					rl_warn("execv(%s): %s", str, strerror(errno));
					exit(EX_DATAERR);
				}
				break;
			case OP_FISH:
				rl_warn("Fish!");
				break;
			case OP_SUID:
				if (rl_debug > 1)
					fprintf(stderr, "++setuid: %d\n", *(op + 1));
				if(setuid(*++op))
					rl_fail1("setuid");
				break;
			case OP_SGID:
				if (rl_debug > 1)
					fprintf(stderr, "++setgid: %d\n", *(op + 1));
				if(setgid(*++op))
					rl_fail1("setgid");
				break;
			case OP_NICE:
				if (rl_debug > 1)
					fprintf(stderr, "++nice: %d\n", *(op + 1));
				if(setpriority(PRIO_PROCESS, 0, *++op))
					rl_fail1("setpriority");
				break;
			case OP_RLIMIT:
				j = *++op;
				k = *++op;

				if(setrlimit(j, rlimittab_get(k)))
					rl_fail1("setrlimit");
				break;
			case OP_CHROOT:
				argv = argvtab_get(*++op);
				string_build(inst, argv);
				if(chroot(argv->str))
					rl_fatal(EX_SOFTWARE, _("ABORT - chroot(\"%s\"): %s"), argv->str, strerror(errno));
				if(chdir("/"))
					rl_fatal(EX_SOFTWARE, _("ABORT - chdir(\"/\"): %s"), strerror(errno));
				break;
			case OP_LOG:
				log = logtab_get(*++op);
				argv = argvtab_get(log->argv);
				if(log->fd < 0) {
					string_build(inst, argv);
					syslog(LOG_INFO, "%s", argv->str);
				} else {
					iov_build(inst, argv);
					if(writev(log->fd, argv->iov, argv->argc) < 0)
						rl_warn("writev() log: %s", strerror(errno));
				}
				break;
			case OP_ACCEPT:
				i = *++op;

				if(inst->sin)
					free(inst->sin);
				inst->sin = NULL;
				inst->sinlen = i;
				if(i) {
					inst->sin = (struct sockaddr *)malloc(i);
					if (!inst->sin)
						rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
				}
				if((inst->sock = accept(inst->sock, inst->sin, &inst->sinlen)) < 0) {
					rl_warn("accept(): %s",	strerror(errno));
					return;
				}
				fcntl(inst->sock, F_SETFL, O_NDELAY);	/* boggle */
				if(fcntl(inst->sock, F_SETFD, 0) == -1) {
					rl_warn("fcntl(%d, F_SETFD, 0): %s", inst->sock, strerror(errno));
				}
				break;
			case OP_FORK:
				k = *++op;
				i = *++op;
				rls_block();
				switch(j = fork()) {
					case -1:
						rl_warn(_("fork() failed - %s"), strerror(errno));
						break;
					case 0:
						rls_unblock();
						all_unhook();
						rlstk_push(&inst->stk, 0);
						run_bytecode(++op, inst);
						exit(rlstk_pop(&inst->stk));
						break;
					default:
						inst->start = time(NULL);
						pidtab_add(j, i, inst);
						rls_unblock();
						return run_bytecode(oplisttab_get(k), inst);
				}
				break;
			case OP_WRAP:
				str = stringtab_get(*++op);
				k = *++op;
#ifdef HAVE_TCP_WRAPPERS
				request = (struct request_info *)malloc(sizeof(struct request_info));
				if (request == NULL)
					rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory (OP: %d)"), *op);

				request_init(request, RQ_FILE, inst->sock,
						RQ_CLIENT_SIN, inst->sin,
						RQ_DAEMON, str,
						0);
				/* undocumented tcp wrappers magic -
				 * does DNS lookup and possibly other stuff */
				fromhost(request);

				if(!hosts_access(request)) {
					char host_ip[100];
					struct loglist l;

					/* get address of client */
					l.type = LOG_SOURCE_IP;
					l.arg = host_ip;
					l.len = sizeof(l.arg);
					loglist_build( inst, &l );

					rl_warn(_("access for service %s from host %s denied by TCP wrappers"), str, host_ip);
					free( request );
					run_bytecode(oplisttab_get(k), inst);
				}
#endif
				break;
			case OP_SETCAP:
#ifdef HAVE_CAPABILITIES
				if(cap_set_proc(captab_get(*++op)))
					rl_warn("cap_set_proc(): %s",	strerror(errno));
#endif
				break;
			case OP_INITGR:
				str = stringtab_get(*++op);
				gid = (gid_t)*++op;
				if (rl_debug > 1)
					fprintf(stderr, "++initgr: %s, %d\n", str, (int)gid);
				if(initgroups(str, gid))
					rl_warn("initgroups(%s, %d): %s", str, (int)gid, strerror(errno));
				break;
			case OP_BRANCH:
				j = *++op;
				op += j;
				break;
			case OP_CLOSE:
				if((inst->sock != -1) && close(inst->sock))
					rl_warn("close(%d): %s", inst->sock, strerror(errno));
				inst->sock = -1;
				return;
			case OP_BUFCOPY:
				rlstk_push(&inst->stk, rlbuf_copy(inst->sock, inst->buf, rlstk_pop(&inst->stk)));
				break;
			case OP_ZERO:
				rlstk_push(&inst->stk, 0);
				break;
			case OP_RET:
				return;
			case OP_EXIT:
				exit(0);
				return;
			case OP_ECHO:
				argv = argvtab_get(*++op);

				iov_build(inst, argv);
				if(writev(inst->sock, argv->iov, argv->argc) < 0)
					rl_warn("writev(): %s",	strerror(errno));
				break;
			case OP_UP:
				sem = semaphore_get(*++op);
				if(sem->count-- == sem->limit)
					run_bytecode(oplisttab_get(sem->under), inst);
				break;
			case OP_DOWN:
				sem = semaphore_get(*++op);
				if(++sem->count == sem->limit)
					run_bytecode(oplisttab_get(sem->match), inst);
				break;
			case OP_FROG:
				rl_warn("Frog!");
				break;
			case OP_LSET:
				listeners_set(*++op);
				break;
			case OP_LCLR:
				listeners_clear(*++op);
				break;
			case OP_BUFINIT:
				rlbuf_init(&inst->buf, *++op);
				break;
			case OP_BUFREAD:
				rlstk_push(&inst->stk, rlbuf_read(inst->sock, inst->buf));
				break;
			case OP_BUFWRITE:
				rlstk_push(&inst->stk, rlbuf_write(inst->sock, inst->buf));
				break;
			case OP_RHOOK:
				++op;
				read_hook(inst->sock, op + 1 + *op, inst);
				return;
			case OP_RUNHOOK:
				read_unhook(inst->sock);
				break;
			case OP_WHOOK:
				++op;
				write_hook(inst->sock, op + 1 + *op, inst);
				return;
			case OP_WUNHOOK:
				write_unhook(inst->sock);
				break;
			case OP_BZ:
				j = *++op;

				if(!rlstk_pop(&inst->stk))
					op += j;
				break;
			case OP_JUMP:
				j = *++op;

				return run_bytecode(oplisttab_get(j), inst);
			case OP_BZNEG:
				j = *++op;

				if(rlstk_pop(&inst->stk) <= 0)
					op += j;
				break;
			case OP_ADD:
				j = rlstk_peek(&inst->stk, 1);

				rlstk_poke(&inst->stk, 0, rlstk_pop(&inst->stk) + j);
				break;
			case OP_SUB:
				j = rlstk_peek(&inst->stk, 1);

				rlstk_poke(&inst->stk, 0, j - rlstk_pop(&inst->stk));
				break;
			case OP_DUP:
				rlstk_push(&inst->stk, rlstk_peek(&inst->stk, 0));
				break;
			case OP_POP:
				rlstk_pop(&inst->stk);
				break;
			case OP_BUFCLONE:
				bt = buftab_get(*++op);

				rlbuf_init(&inst->buf, 0);
				inst->buf->data = bt->addr;
				inst->buf->size = inst->buf->head = bt->len;
				break;
			case OP_BUFFREE:
				inst->buf->data = NULL;
				inst->buf->size = 0;
				break;
			default:
																/* should never happen */
				rl_fatal(EX_SOFTWARE, _("ABORT - Unknown opcode (%d)"), *op);
				break;
		}
	} while(op++);
}

#ifdef BYTECODE_DEBUG
static char * bytecode2string(rl_opcode_t op)
{
	switch(op) {

		case OP_EXEC    : return "OP_EXEC";
		case OP_FISH    : return "OP_FISH";
		case OP_SUID    : return "OP_SUID";
		case OP_SGID    : return "OP_SGID";
		case OP_NICE    : return "OP_NICE";
		case OP_RLIMIT  : return "OP_RLIMIT";
		case OP_LOG     : return "OP_LOG";
		case OP_CHROOT  : return "OP_CHROOT";
		case OP_ACCEPT  : return "OP_ACCEPT";
		case OP_FORK    : return "OP_FORK";
		case OP_WRAP    : return "OP_WRAP";
		case OP_SETCAP  : return "OP_SETCAP";
		case OP_INITGR  : return "OP_INITGR";
		case OP_BRANCH  : return "OP_BRANCH";
		case OP_CLOSE   : return "OP_CLOSE";
		case OP_BUFCOPY : return "OP_BUFCOPY";
		case OP_ZERO    : return "OP_ZERO";
		case OP_RET     : return "OP_RET";
		case OP_EXIT    : return "OP_EXIT";
		case OP_ECHO    : return "OP_ECHO";
		case OP_UP      : return "OP_UP";
		case OP_DOWN    : return "OP_DOWN";
		case OP_FROG    : return "OP_FROG";
		case OP_LSET    : return "OP_LSET";
		case OP_LCLR    : return "OP_LCLR";
		case OP_BUFINIT : return "OP_BUFINIT";
		case OP_BUFREAD : return "OP_BUFREAD";
		case OP_BUFWRITE: return "OP_BUFWRITE";
		case OP_RHOOK   : return "OP_RHOOK";
		case OP_RUNHOOK : return "OP_RUNHOOK";
		case OP_WHOOK   : return "OP_WHOOK";
		case OP_WUNHOOK : return "OP_WUNHOOK";
		case OP_BZ      : return "OP_BZ";
		case OP_JUMP    : return "OP_JUMP";
		case OP_BZNEG   : return "OP_BZNEG";
		case OP_ADD     : return "OP_ADD";
		case OP_SUB     : return "OP_SUB";
		case OP_DUP     : return "OP_DUP";
		case OP_POP     : return "OP_POP";
		case OP_BUFCLONE: return "OP_BUFCLONE";
		case OP_BUFFREE : return "OP_BUFFREE";
		default         : return "UNKNOWN";
	}
}
static char *
bytecodeargs2string (rl_opcode_t * op)
{
	int nargs;

	switch (*op)
	{
		case OP_ACCEPT:
		case OP_BRANCH:
		case OP_BUFCLONE:
		case OP_BUFCOPY:
		case OP_BUFINIT:
		case OP_BZNEG:
		case OP_BZ:
		case OP_CHROOT:
		case OP_DOWN:
		case OP_ECHO:
		case OP_JUMP:
		case OP_LCLR:
		case OP_LOG:
		case OP_LSET:
		case OP_NICE:
		case OP_RHOOK:
		case OP_SETCAP:
		case OP_SGID:
		case OP_SUID:
		case OP_UP:
		case OP_WHOOK:
			nargs = 1;
			break;

		case OP_EXEC:
		case OP_FORK:
		case OP_INITGR:
		case OP_RLIMIT:
		case OP_WRAP:
			nargs = 2;
			break;
		default:
			nargs = 0;
			break;
	}

	static char buf[128];
	int i;
	buf[0] = 0;

	for (i = 0; i < nargs; ++i)
	{
			sprintf (buf + strlen (buf), "%s%d", i ? ", " : "", *(op + i + 1));
	}
	return buf;

};

#endif

/* vim: set ts=2: */
