#include "config.h"

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include "port/port.h"
#include "data.h"
#include "db.h"
#include "libdb.h"
#include "error.h"
#include "rlinetd.h"
#include "util.h"

#ifndef MAP_FAILED
#define MAP_FAILED ((void *)-1)
#endif

static void loglist_append(int idx, int type, char *arg, int len);
int loglist_parse(int idx, char c);

static void logtab_grow(void) {
	logtabs = (struct logtab *)realloc(logtabs, ++numlogtabs * sizeof(struct logtab));
	if (!logtabs)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(logtabs + numlogtabs - 1, 0, sizeof(struct logtab));
	logtabs[numlogtabs - 1].fd = -1;
}

static char escape_lookup(char c) {
	switch(c) {
		case 'n':
			return '\n';
		case 'r':
			return '\r';
		case 't':
			return '\t';
		default:
			return c;
	}
}

int logtab_add(int fd, char *arg) {
	int len, i, text = 0, idx;
	char *start;

	idx = numargvs;
	argvtab_grow();
	len = strlen(arg);
	start = arg;
	for(i = 0; i < len; i++) {
		if(arg[i] == '%') {
			arg[i] = '\0';
			if(text)
				loglist_append(idx, LOG_TEXT, start, strlen(start));
			start = arg + i + 2;
			text = 0;
			if(loglist_parse(idx, arg[++i])) {
				start -= 1;
				text = 1;
			}
		}
		if(arg[i] == '\\') {
			arg[i] = '\0';
			if(text)
				loglist_append(idx, LOG_TEXT, start, strlen(start));
			start = arg + ++i;
			arg[i] = escape_lookup(arg[i]);
		}
		text = 1;
	}
	if(text)
		loglist_append(idx, LOG_TEXT, start, strlen(start));
	loglist_append(idx, LOG_TEXT, rl_lf, 1);
	i = numlogtabs;
	logtab_grow();
	logtabs[i].argv = idx;
	logtabs[i].fd = fd;
	return i;
}

void logtabs_free(void) {
	if(logtabs)
		free(logtabs);
	logtabs = NULL;
	numlogtabs = 0;
}

static void loglist_grow(struct argvtab *argv) {
	argv->ents = (struct loglist *)
		realloc(argv->ents, ++argv->argc * sizeof(struct loglist));
	if (!argv->ents)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));

}

static void loglist_append(int idx, int type, char *arg, int len) {
	int i = argvs[idx].argc;

	loglist_grow(argvs + idx);
	argvs[idx].ents[i].type = type;
	argvs[idx].ents[i].len = len;
	argvs[idx].ents[i].arg = len ? malloc(len + 1) : NULL;
	if(arg && len) {
		strncpy(argvs[idx].ents[i].arg, arg, len);
		argvs[idx].ents[i].arg[len] = '\0';
	}
}

int loglist_parse(int idx, char c) {
	switch(c) {
		case '%':
			return 1;
		case 'O':								/* origin IP */
			loglist_append(idx, LOG_SOURCE_IP, NULL, 80);
			break;
		case 'P':
			loglist_append(idx, LOG_SOURCE_PORT, NULL, 6);
			break;
		case 'C':								/* cpu time */
			loglist_append(idx, LOG_CPU, NULL, 10);
			break;
		case 'U':
			loglist_append(idx, LOG_CPU_USER, NULL, 10);
			break;
		case 'S':
			loglist_append(idx, LOG_CPU_SYSTEM, NULL, 10);
			break;
		case 'r':
			loglist_append(idx, LOG_RSS, NULL, 10);
			break;
		case 'm':
			loglist_append(idx, LOG_SHMEM, NULL, 10);
			break;
		case 'd':
			loglist_append(idx, LOG_DATA, NULL, 10);
			break;
		case 's':
			loglist_append(idx, LOG_STACK, NULL, 10);
			break;
		case 'f':
			loglist_append(idx, LOG_MINFLT, NULL, 10);
			break;
		case 'F':
			loglist_append(idx, LOG_MAJFLT, NULL, 10);
			break;
		case 'p':
			loglist_append(idx, LOG_SWAPS, NULL, 10);
			break;
		case 'i':
			loglist_append(idx, LOG_BIN, NULL, 10);
			break;
		case 'o':
			loglist_append(idx, LOG_BOUT, NULL, 10);
			break;
		case 'n':
			loglist_append(idx, LOG_MOUT, NULL, 10);
			break;
		case 'c':
			loglist_append(idx, LOG_MIN, NULL, 10);
			break;
		case 'k':
			loglist_append(idx, LOG_NSIG, NULL, 10);
			break;
		case 'w':
			loglist_append(idx, LOG_VCSW, NULL, 10);
			break;
		case 'W':
			loglist_append(idx, LOG_IVCSW, NULL, 10);
			break;
		case 'e':
			loglist_append(idx, LOG_EXITCODE, NULL, 6);
			break;
		case 't':
			loglist_append(idx, LOG_TIME, NULL, 10);
			break;
		case 'M':
			loglist_append(idx, LOG_ATIME, NULL, 5);
			break;
		case 'I':
			loglist_append(idx, LOG_CTIME, NULL, 35);
			break;
		default:
			rl_warn("unknown log modifier %%%c", c);
	}
	return 0;
}


int argvtab_add(char *arg, int split_on_spaces) {
	int len, i, text = 0, idx;
	char *start;

	idx = numargvs;
	argvtab_grow();
	if(!arg)
		rl_fatal(EX_SOFTWARE, _("No exec string passed to argvtab_add()"));
	len = strlen(arg);
	start = arg;
	i = 0;
	while(i < len) {
		if(split_on_spaces && (isspace(arg[i]))) {
			arg[i++] = '\0';
			if(text)
				loglist_append(idx, LOG_TEXT, start, strlen(start));
			while(isspace(arg[i]) && (i < len))
				i++;
			start = arg + i;
			text = 0;
			continue;
		} else
		if(arg[i] == '\\') {
			arg[i] = '\0';
			if(text)
				loglist_append(idx, LOG_TEXT, start, strlen(start));
			start = arg + ++i;
			arg[i] = escape_lookup(arg[i]);
			i++;
		} else
		if(arg[i] == '%') {
			arg[i] = '\0';
			if(text)
				loglist_append(idx, LOG_TEXT, start, strlen(start));
			start = arg + i + 2;
			text = 0;
			if(loglist_parse(idx, arg[++i])) {
				start -= 1;
				text = 1;
			}
			i++;
			continue;
		} else {
			i++;
			text = 1;
		}
	}
	if(text)
		loglist_append(idx, LOG_TEXT, start, strlen(start));
	free(arg);
	return idx;
}

static void rlimittab_grow(void) {
	rlimits = (struct rlimit *)
		realloc(rlimits, ++numrlimits * sizeof(struct rlimit));
	if (!rlimits)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
}

void rlimittabs_free(void) {
	if(rlimits)
		free(rlimits);
	numrlimits = 0;
	rlimits = NULL;
}


int rlimittab_add(rlim_t soft, rlim_t hard) {
	int i = numrlimits;

	rlimittab_grow();
	rlimits[i].rlim_cur = soft;
	rlimits[i].rlim_max = hard;
	return i;
}

void argvtabs_free(void) {
	int i, j;
	
	for(j = 0; j < numargvs; j++) {
		for(i = 0; i < argvs[j].argc; i++) {
			if(argvs[j].ents[i].arg) {
				argvs[j].ents[i].len = 0;
				free(argvs[j].ents[i].arg);
			}
		}
		if(argvs[j].ents)
			free(argvs[j].ents);
		if(argvs[j].argv)
			free(argvs[j].argv);
		if(argvs[j].iov)
			free(argvs[j].iov);
		if(argvs[j].str)
			free(argvs[j].str);
		argvs[j].argc = 0;
	}
	if(argvs)
		free(argvs);
	numargvs = 0;
	argvs = NULL;
}
		
void argvtab_grow() {
	argvs = (struct argvtab *)realloc(argvs, ++numargvs * sizeof(struct argvtab));
	if (!argvs)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(argvs + numargvs - 1, 0, sizeof(struct argvtab));
}

#ifdef HAVE_CAPABILITIES

static void captab_grow(void) {
	caps = (cap_t *)realloc(caps, ++numcaps * sizeof(cap_t));
	if (!caps)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(caps + numcaps - 1, 0, sizeof(cap_t));
}

int captab_add(cap_t cap) {
	int idx = numcaps;

	captab_grow();
	caps[idx] = cap;
	return idx;
}

void captabs_free() {
	int i;

	for(i = 0; i < numcaps; i++)
		cap_free(caps + i);
	if(caps)
		free(caps);
	caps = NULL;
	numcaps = 0;
}

#endif /* HAVE_CAPABILITIES */

static void stringtab_grow(void) {
	strings = (char **)realloc(strings, ++numstrings * sizeof(char *));
	if (!strings)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(strings + numstrings - 1, 0, sizeof(char *));
}

int stringtab_add(char *str) {
	int idx;

	for(idx = 0; idx < numstrings; idx++)
		if(!strcmp(strings[idx], str))
			return idx;
	stringtab_grow();
	strings[idx] = strdup(str);
	return idx;
}

void stringtabs_free() {
	int i;

	for(i = 0; i < numstrings; i++)
		if(strings[i])
			free(strings[i]);
	free(strings);
	strings = NULL;
	numstrings = 0;
}

static void buftab_grow(void) {
	bufs = (struct buftab *)realloc(bufs, ++numbufs * sizeof(struct buftab));
	if (!bufs)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(bufs + numbufs - 1, 0, sizeof(struct buftab));
}

int buftab_addbuf(const char *buf, int len) {
	int idx = numbufs;

	buftab_grow();
	bufs[idx].addr = malloc(len);
	if (!bufs[idx].addr)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memcpy(bufs[idx].addr, buf, len);
	bufs[idx].len = len;
	return idx;
}

int buftab_addfile(char *path) {
	int idx = numbufs;
	void *addr;
	int len;

	if(rl_readfile(path, &addr, &len))
		return -1;
	buftab_grow();
	bufs[idx].addr = addr;
	bufs[idx].len = len;
	return idx;
}

void buftabs_free() {
	int i;

	for(i = 0; i < numbufs; i++) {
		free(bufs[i].addr);
	}
	free(bufs);
	bufs = NULL;
	numbufs = 0;
}

static void oplisttab_grow(void) {
	oplists = (struct oplist *)realloc(oplists, ++numoplists * sizeof(struct oplist));
	if (!oplists)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(oplists + numoplists - 1, 0, sizeof(struct oplist));
}

static void oplist_copy(struct oplist *to, struct oplist *from) {
	int len = sizeof(rl_opcode_t) * from->ops_len;

	to->ops_len = from->ops_len;
	if(to->ops_len && len) {
		to->ops_list = (rl_opcode_t *)malloc(len);
		if (!to->ops_list)
			rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
		memcpy(to->ops_list, from->ops_list, len);
	}
}

int oplisttab_add(struct oplist *op) {
	int i;

	for(i = 0; i < numoplists; i++)
		if(oplists[i].ops_len == op->ops_len)
			if(!memcmp(oplists[i].ops_list, op->ops_list, op->ops_len * sizeof(rl_opcode_t)))
				return i;
	oplisttab_grow();

	oplist_copy(oplists + i, op);
	return i;
}

void oplist_free(struct oplist *o) {
	if(o->ops_list) {
		free(o->ops_list);
		o->ops_list = NULL;
	}
	o->ops_len = 0;
}
	
void oplisttabs_free() {
	int i;

	for(i = 0; i < numoplists; i++)
		oplist_free(oplists + i);
	free(oplists);
	oplists = NULL;
	numoplists = 0;
}

void semaphores_free() {
	free(sems);
	sems = NULL;
	numsems = 0;
}

static void semaphore_grow(void) {
	sems = (struct semaphore *)realloc(sems, ++numsems * sizeof(*sems));
	if (!sems)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(sems + numsems - 1, 0, sizeof(*sems));
}

int semaphore_add(int limit, rl_opcode_t match, rl_opcode_t under) {
	int idx = numsems;
	
	semaphore_grow();
	sems[idx].limit = limit;
	sems[idx].match = match;
	sems[idx].under = under;
	return idx;
}

static void fdsettab_grow(void) {
	fdsets = (fd_set *)realloc(fdsets, ++numfdsets * sizeof(*fdsets));
	if (!fdsets)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(fdsets + numfdsets - 1, 0, sizeof(*fdsets));
}

int fdsettab_add(fd_set *fds) {
	fdsettab_grow();
	memcpy(fdsets + numfdsets - 1, fds, sizeof(*fds));
	return numfdsets - 1;
}

void fdsettabs_free() {
	free(fdsets);
	fdsets = NULL;
	numfdsets = 0;
}

/* vim: set ts=2: */
