#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

#include <pwd.h>

#include "port/port.h"

#include "data.h"
#include "db.h"
#include "error.h"

void numlist_add(struct numlist **list, long num) {
	struct numlist *tmp = (struct numlist *)malloc(sizeof(*tmp));

	if(!tmp)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	tmp->next = *list;
	tmp->num = num;
	*list = tmp;
}

void numlist_copy(struct numlist **to, struct numlist *from) {
	while(from) {
		numlist_add(to, from->num);
		from = from->next;
	}
}

void numlist_free(struct numlist *n) {
	struct numlist *p;

	while(n) {
		p = n->next;
		free(n);
		n = p;
	}
}

void stringlist_add(struct stringlist **list, char *str) {
	struct stringlist *tmp = (struct stringlist *)malloc(sizeof(*tmp));

	if(!tmp)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	tmp->next = *list;
	tmp->str = str;
	*list = tmp;
}

void stringlist_copy(struct stringlist **to, struct stringlist *from) {
	while(from) {
		stringlist_add(to, strdup(from->str));
		from = from->next;
	}
}

void stringlist_free(struct stringlist *n) {
	struct stringlist *p;

	while(n) {
		p = n->next;
		if(n->str)
			free(n->str);
		free(n);
		n = p;
	}
}

void builduserdata(struct userdata **dest, char *name) {
	struct passwd pw;
	struct passwd *pp;

	pp = &pw;
	pp = getpwnam(name);
	endpwent();
	if(!pp)
		return;
	clearuserdata(dest);
	(*dest)->name = name;
	(*dest)->uid = pp->pw_uid;
	(*dest)->gid = pp->pw_gid;
}

void clearuserdata(struct userdata **dest) {
	if(!*dest)
		return;
	if((*dest)->name)
		free((*dest)->name);
	memset(*dest, 0, sizeof(**dest));
	(*dest)->uid = -1;
	(*dest)->gid = -1;
}

void newuserdata(struct userdata **dest) {
	if(!*dest)
		*dest = (struct userdata *)malloc(sizeof(**dest));
	if(!*dest)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(*dest, 0, sizeof(**dest));
	(*dest)->uid = -1;
	(*dest)->gid = -1;
}

void userdata_copy(struct userdata **to, struct userdata *from) {
	if(!from)
		return;
	newuserdata(to);
	memcpy(*to, from, sizeof(**to));
	(*to)->name = from->name ? strdup(from->name) : NULL;
}


/* vim: set ts=2: */
