#include "config.h"

#include <ctype.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sysexits.h>

#ifdef HAVE_CAPABILITIES
#include <sys/capability.h>
#endif

#include "db.h"
#include "rlinetd.h"
#include "strings.h"
#include "error.h"

struct argvtab *argvs = NULL;
int numargvs = 0;

struct rlimit *rlimits = NULL;
int numrlimits = 0;

struct logtab *logtabs = NULL;
int numlogtabs = 0;

char **strings = NULL;
int numstrings = 0;

struct buftab *bufs = NULL;
int numbufs = 0;

struct oplist *oplists = NULL;
int numoplists = 0;

struct pidtab pidtabs[8] = {
	{ 0, NULL, NULL, NULL, 0, NULL },
	{ 0, NULL, NULL, NULL, 0, NULL },
	{ 0, NULL, NULL, NULL, 0, NULL },
	{ 0, NULL, NULL, NULL, 0, NULL },
	{ 0, NULL, NULL, NULL, 0, NULL },
	{ 0, NULL, NULL, NULL, 0, NULL },
	{ 0, NULL, NULL, NULL, 0, NULL },
	{ 0, NULL, NULL, NULL, 0, NULL }
};

struct semaphore *sems = NULL;
int numsems = 0;

#ifdef HAVE_CAPABILITIES
cap_t *caps;
int numcaps;
#endif

fd_set *fdsets = NULL;
int numfdsets = 0;


static struct pidtab *pidtab_new(void) {
	struct pidtab *p;
	
	p = (struct pidtab *)malloc(sizeof(struct pidtab));
	if (!p)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(p, 0, sizeof(struct pidtab));
	return p;
}

void pidtab_add(pid_t pid, int onexit, struct rl_instance *inst) {
	struct pidtab *p = pidtab_new();
	p->pid = pid;
	p->inst = inst;
	p->onexit = onexit;
	p->prev = pidtabs + (pid & 0x7);
	p->next = p->prev->next;
	p->prev->next = p;
	if(p->next)
		p->next->prev = p;
}

#ifdef BYTECODE_DEBUG
#define CHECK(tab,print) \
  fprintf(stderr, #tab " get %d, %d\n", i, num##tab); \
	if (i < 0 || i >= num##tab) { fprintf(stderr, #tab ", %d >= %d\n",  i, num##tab); return 0; } \
	else if (print)    fprintf(stderr, #tab "[%d] = %s\n", i, tab[i]); \
	fflush(stderr);
#else
#define CHECK(tab,print)
#endif

struct pidtab *pidtab_get(pid_t pid) {
	struct pidtab *p;

	p = pidtabs[pid & 0x7].next;
	if(!p)
		return NULL;
	do {
		if(p->pid == pid) {
			return p;
		}
	} while((p = p->next));
	return NULL;
}

struct logtab *logtab_get(int i) {
	CHECK(logtabs, 0)
	return logtabs + i;
}

struct rlimit *rlimittab_get(int i) {
	CHECK(rlimits, 0)
	return rlimits + i;
}

struct argvtab *argvtab_get(int i) {
	CHECK(argvs, 0)
	return argvs + i;
}

#ifdef HAVE_CAPABILITIES

cap_t captab_get(int i) {
	return caps[i];
}

#endif /* HAVE_CAPABILITIES */


char *stringtab_get(int i) {
	CHECK(strings, 1)
	return strings[i];
}

struct buftab *buftab_get(int i) {
	CHECK(bufs, 1)
	return bufs + i;
}

rl_opcode_t *oplisttab_get(int i) {
	CHECK(oplists, 0)
	return oplists[i].ops_list;
}

struct semaphore *semaphore_get(int i) {
	CHECK(sems, 0)
	return sems + i;
}

fd_set *fdsettab_get(int i) {
	CHECK(fdsets, 0)
	return fdsets + i;
}

/* vim: set ts=2: */
