#ifndef H_ERROR
#define H_ERROR

#include <errno.h>
#include <string.h>
#include <sysexits.h>

void rl_note(const char *, ...);

void rl_warn(const char *, ...);
void rl_fatal(int, const char *, ...);
	
void rl_pwarn(const char *, int, const char *, ...);
void rl_pfatal(int, const char *, int,  const char *, ...);

#endif /* H_ERROR */

/* vim: set ts=2: */
