#include "config.h"

#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif

#include "bytecode.h"
#include "db.h"
#include "engine.h"
#include "error.h"
#include "rlinetd.h"
#include "signals.h"

struct rl_cleanup *rl_cleanups = NULL;
int hisock;
extern char *rl_parser;
struct pidtab *rl_pending;
struct rl_instance *inst_new(void);

struct fd_ops {
	rl_opcode_t *op;
	struct rl_instance *inst;
};

fd_set rfds, wfds;
struct fd_ops *rfd_ops = NULL, *wfd_ops = NULL;
int rfd_len = 0, wfd_len = 0;

/* runs the specified function from our parser library */
static void run_library_func(const char* const funcname) {
	int i;				
	void *handle;
  void (*libfunc)(void);
	
  if((handle = dlopen(rl_parser, RTLD_NOW
#ifdef RTLD_GLOBAL
											| RTLD_GLOBAL
#endif
											))) {
		if((libfunc = dlsym(handle, funcname))) {
			int bound;
						
			closelog();
			i = 3;
			bound = FD_SETSIZE;
			for(; i < bound; i++)
					close(i);
			libfunc();
		} else {
			rl_fatal(EX_SOFTWARE, _("ABORT - failed to find function \"%s\" in parser module %s (%s)"),
						 funcname, rl_parser, dlerror());
		}
	} else {
					rl_fatal(EX_SOFTWARE, _("ABORT - failed to load parser module %s (%s)"),
									 rl_parser, dlerror());
	}
	dlclose(handle); 
}				

void main_loop(void) {
	fd_set srfds, swfds;
	
	for(;;) {
		int i, n;
#ifdef HAVE_MALLINFO
		static long oldmem;
		struct mallinfo m;
#endif		

		if (rls_term_recv) { /* received SIGTERM or SIGINT */
			run_library_func("services_free");
			return;
		}	
						
		if(rls_need_parse || rls_reaped) {
			rls_block();
			if(rls_need_parse) {
#ifdef HAVE_MALLINFO
				m = mallinfo();
#endif
				run_library_func("parse");
				rls_need_parse = 0;
#ifdef HAVE_MALLINFO
				m = mallinfo();
				rl_warn(_("rlinetd configuration (re)loaded, %ld bytes used\n"),
								m.uordblks - oldmem);
				oldmem = m.uordblks;
#else
				rl_warn(_("rlinetd configuration (re)loaded"));
#endif
			}
			if(rls_reaped) {
				struct pidtab *p;
				
				rl_pending = rls_reaped;
				rls_reaped = NULL;
				p = rl_pending;
				do {
					if(p->next)
						p->next->prev = p->prev;
					if(p->prev)
						p->prev->next = p->next;
					p->next = NULL;
					p->prev = NULL;
				} while((p = p->next_cleanup));
			}
			rls_unblock();
		}
			
		while(rl_pending) {
			struct pidtab *p;
			
			run_bytecode(oplisttab_get(rl_pending->onexit), rl_pending->inst);
			if(rl_pending->inst)
				inst_free(rl_pending->inst);
			p = rl_pending->next_cleanup;
			free(rl_pending);
			rl_pending = p;
		}
		srfds = rfds;
		swfds = wfds;
		if((n = select(hisock + 1, &srfds, &swfds, NULL, NULL)) < 0) {
			switch(errno) {
				case EINTR:
					break;
				default:
					rl_warn(_("select() failed - %s"), strerror(errno));
					break;
			}
			continue;
		}
		if(!n)
			continue;
		for(i = 0; i <= hisock; i++) {
			if(FD_ISSET(i, &srfds) && FD_ISSET(i, &rfds)) {
				struct rl_instance *grr;

				grr = rfd_ops[i].inst;
				if(!rfd_ops[i].inst) {
					grr = inst_new();
					grr->sock = i;
				}
				run_bytecode(rfd_ops[i].op, grr);
			}
			if(FD_ISSET(i, &swfds) && FD_ISSET(i, &wfds))
				run_bytecode(wfd_ops[i].op, wfd_ops[i].inst);
		}
	}
}

void listeners_set(int j) {
	fd_set *fds;
	int i;

	fds = fdsettab_get(j);
	for(i = 0; i < FD_SETSIZE; i++)
		if(FD_ISSET(i, fds))
			FD_SET(i, &rfds);
}

void listeners_clear(int j) {
	fd_set *fds;
	int i;

	fds = fdsettab_get(j);
	for(i = 0; i < FD_SETSIZE; i++)
		if(FD_ISSET(i, fds))
			FD_CLR(i, &rfds);
}


static void fdops_grow(struct fd_ops **ops, int *len, int new) {
	if(new < *len)
		return;
	*ops = realloc(*ops, (new + 1) * sizeof(**ops));
	if (!*ops)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	*len = new + 1;
}

void read_hook(int fd, rl_opcode_t *op, struct rl_instance *inst) {
	fdops_grow(&rfd_ops, &rfd_len, fd);
	rfd_ops[fd].op = op;
	rfd_ops[fd].inst = inst;
	FD_SET(fd, &rfds);
	if(fd > hisock)
		hisock = fd;
}

void read_unhook(int fd) {
	fdops_grow(&rfd_ops, &rfd_len, fd);
	FD_CLR(fd, &rfds);
}

void write_hook(int fd, rl_opcode_t *op, struct rl_instance *inst) {
	fdops_grow(&wfd_ops, &wfd_len, fd);
	wfd_ops[fd].op = op;
	wfd_ops[fd].inst = inst;
	FD_SET(fd, &wfds);
	if(fd > hisock)
		hisock = fd;
}

void write_unhook(int fd) {
	fdops_grow(&wfd_ops, &wfd_len, fd);
	FD_CLR(fd, &wfds);
}

void all_unhook() {
	if(rfd_ops)
		free(rfd_ops);
	rfd_ops = NULL;
	rfd_len = 0;
	if(wfd_ops)
		free(wfd_ops);
	wfd_ops = NULL;
	wfd_len = 0;
	FD_ZERO(&rfds);
	FD_ZERO(&wfds);
}

struct rl_instance *inst_new(void) {
	struct rl_instance *i;

	i = malloc(sizeof(*i));
	if (!i)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(i, 0, sizeof(*i));
	return i;
}

void inst_free(struct rl_instance *i) {
	if(i->sin)
		free(i->sin);
	if(i->buf) {
		if(i->buf->data)
			free(i->buf->data);
		free(i->buf);
	}
	free(i);
}

/* vim: set ts=2: */
