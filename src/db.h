#ifndef H_DB
#define H_DB

#include <sys/types.h>
#include <grp.h>
#include <netinet/in.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include "config.h"

#ifdef HAVE_CAPABILITIES
#include <sys/capability.h>
#endif

#include "rlinetd.h"

struct loglist {
	int type;
	char *arg;
	int len;
};

#include "strings.h"

enum { LOG_TEXT, LOG_SOURCE_IP, LOG_SOURCE_PORT, LOG_CPU,
			 LOG_CPU_USER, LOG_CPU_SYSTEM, LOG_RSS, LOG_SHMEM, LOG_DATA, LOG_STACK,
			 LOG_MINFLT, LOG_MAJFLT, LOG_SWAPS, LOG_BIN, LOG_BOUT, LOG_MOUT,
			 LOG_MIN, LOG_NSIG, LOG_VCSW, LOG_IVCSW, LOG_EXITCODE, LOG_TIME,
			 LOG_ATIME, LOG_CTIME
};

void pidtab_add(pid_t, int, struct rl_instance *);
struct pidtab *pidtab_get(pid_t);
int logtab_add(int, char *);
struct logtab *logtab_get(int);
int rlimittab_add(rlim_t, rlim_t);
struct rlimit *rlimittab_get(int);
void argvtab_grow(void);
struct argvtab *argvtab_get(int);
struct iovec *iov_build(struct rl_instance *, struct argvtab *);
char **argv_build(struct rl_instance *, struct argvtab *);
int argvtab_add(char *, int);

#ifdef HAVE_CAPABILITIES
int captab_add(cap_t);
void captabs_free(void);
cap_t captab_get(int);
#endif

int stringtab_add(char *);
char *stringtab_get(int);
void stringtabs_free(void);
void addrtabs_free(void);
int addrtab_add(void *addr);
void addrtab_grow(void);

struct oplist {
	int ops_len;
	rl_opcode_t *ops_list;
};

int oplisttab_add(struct oplist *);
void oplisttabs_free(void);
rl_opcode_t *oplisttab_get(int);
void oplist_free(struct oplist *op);

struct logtab {
	int fd;
	int argv;
	char *path;
};

extern struct logtab *logtabs;
extern int numlogtabs;

extern struct argvtab *argvs;
extern int numargvs;

extern struct rlimit *rlimits;
extern int numrlimits;

#ifdef HAVE_CAPABILITIES
extern cap_t *caps;
extern int numcaps;
#endif

extern char **strings;
extern int numstrings;

struct buftab {
	void *addr;
	size_t len;
};

void buftabs_free(void);
int buftab_addfile(char *);
int buftab_addbuf(const char *, int);
struct buftab *buftab_get(int);

extern struct buftab *bufs;
extern int numbufs;

extern struct oplist *oplists;
extern int numoplists;

extern char rl_lf[];

struct pidtab {
	pid_t pid;
	struct pidtab *next;
	struct pidtab *prev;
	struct pidtab *next_cleanup;
	int onexit;
	struct rl_instance *inst;
};

extern struct pidtab pidtabs[8];

struct semaphore {
	int limit;										/* could do hi/lo water marks */
	int count;
	rl_opcode_t match;						/* executed when count reaches limit */
	rl_opcode_t under;						/* executed when count drops below limit */
};

extern struct semaphore *sems;
extern int numsems;

struct semaphore *semaphore_get(int);
int semaphore_add(int, rl_opcode_t, rl_opcode_t);
void semaphores_free(void);

extern fd_set *fdsets;
extern int numfdsets;

int fdsettab_add(fd_set *);
void fdsettabs_free(void);
fd_set *fdsettab_get(int);

#endif /* !H_DB */

/* vim: set ts=2: */
