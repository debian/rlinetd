#ifndef H_STACK
#define H_STACK

#define STACKSIZE 8

struct rl_stack {
	rl_opcode_t data[STACKSIZE];
	int top;
};

void rlstk_push(struct rl_stack *, rl_opcode_t);
rl_opcode_t rlstk_pop(struct rl_stack *);
rl_opcode_t rlstk_peek(struct rl_stack *, int);
void rlstk_poke(struct rl_stack *, int, rl_opcode_t);
struct rl_stack *rlstk_new(void);
void rlstk_free(struct rl_stack *);

#endif /* !H_STACK */

/* vim: set ts=2: */
