#ifndef H_SIGNALS
#define H_SIGNALS

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

void rl_siginit(void);
void rls_block(void);
void rls_unblock(void);

#define RL_MAX(a,b) ((a) > (b) ? (a) : (b))

extern volatile int rls_need_parse;					/* SIGHUP flag */
extern volatile int rls_term_recv;					/* SIGTERM or SIGINT flag */
extern struct pidtab * volatile rls_reaped;

#endif /* !H_SIGNALS */

/* vim: set ts=2: */
