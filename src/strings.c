#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#include "db.h"
#include "error.h"
#include "rlinetd.h"
#include "strings.h"

static void loglist_walk(struct rl_instance *inst, struct loglist *l, int c) {
	int i;

	for(i = 0; i < c; i++)
		loglist_build(inst, l + i);
}

static void string_populate(struct argvtab *a) {
	int i, len;

	for(len = 0, i = 0; i < a->argc; i++)
		len += a->ents[i].len;
	a->str = malloc(len + 1);
	if (!a->str)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
}

void string_build(struct rl_instance *inst, struct argvtab *a) {
	int i;

	if(!a->str)
		string_populate(a);
	a->str[0] = '\0';
	for(i = 0; i < a->argc; i++) {
		loglist_build(inst, a->ents + i);
		strncat(a->str, a->ents[i].arg, a->ents[i].len);
	}
}

static void argv_populate(struct argvtab *a) {
	int i;
	
	a->argv = (char **)calloc(a->argc + 1, sizeof(char *));
	if (!a->argv)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	for(i = 0; i < a->argc; i++)
		a->argv[i] = a->ents[i].arg;
	a->argv[i] = NULL;
}

char **argv_build(struct rl_instance *i, struct argvtab *a) {
	if(!a->argv)
		argv_populate(a);
	loglist_walk(i, a->ents, a->argc);
	return a->argv;
}

static void iov_populate(struct argvtab *a) {
	int i;
	
	a->iov = (struct iovec *)calloc(a->argc, sizeof(struct iovec));
	if (!a->iov)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	for(i = 0; i < a->argc; i++)
		a->iov[i].iov_base = a->ents[i].arg;
}

struct iovec *iov_build(struct rl_instance *inst, struct argvtab *a) {
	int i;

	if(!a->iov)
		iov_populate(a);
	for(i = 0; i < a->argc; i++) {
		loglist_build(inst, a->ents + i);
		a->iov[i].iov_len = a->ents[i].len;
	}
	return a->iov;
}
	
void loglist_build(struct rl_instance *inst, struct loglist *l) {
	time_t tt;
	
	switch(l->type) {
		case LOG_TEXT:
			return;
		case LOG_SOURCE_IP:
			if(!inst->sin) {
				strcpy(l->arg, "<unknown>");
				l->len = strlen(l->arg);
				return;
			}
#ifdef HAVE_INET_NTOP
			if(((struct sockaddr_in *)inst->sin)->sin_family == PF_INET) 
				inet_ntop(PF_INET, &((struct sockaddr_in *)inst->sin)->sin_addr,
									l->arg, 80);
#ifdef HAVE_SOCKADDR_IN6
			else
				inet_ntop(PF_INET6, &((struct sockaddr_in6 *)inst->sin)->sin6_addr,
									l->arg, 80);
#endif
#else			
			strncpy(l->arg, inet_ntoa(((struct sockaddr_in *)inst->sin)->sin_addr), 80);
#endif			
			l->len = strlen(l->arg);
			return;
		case LOG_SOURCE_PORT:
			if(!inst->sin) {
				strcpy(l->arg, "<unknown>");
				l->len = strlen(l->arg);
				return;
			}
			l->len = snprintf(l->arg, 6, "%d", ntohs(((struct sockaddr_in *)inst->sin)->sin_port));
			return;
		case LOG_CPU:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_utime.tv_sec +
								 inst->rusage.ru_stime.tv_sec);
			return;
		case LOG_CPU_USER:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_utime.tv_sec);
			return;
		case LOG_CPU_SYSTEM:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_stime.tv_sec);
			return;
		case LOG_RSS:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_maxrss);
			return;
		case LOG_SHMEM:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_ixrss);
			return;
		case LOG_DATA:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_idrss);
			return;
		case LOG_STACK:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_isrss);
			return;
		case LOG_MINFLT:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_minflt);
			return;
		case LOG_MAJFLT:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_majflt);
			return;
		case LOG_SWAPS:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_nswap);
			return;
		case LOG_BIN:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_inblock);
			return;
		case LOG_BOUT:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_oublock);
			return;
		case LOG_MOUT:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_msgsnd);
			return;
		case LOG_MIN:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_msgrcv);
			return;
		case LOG_NSIG:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_nsignals);
			return;
		case LOG_VCSW:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_nvcsw);
			return;
		case LOG_IVCSW:
			l->len =
				snprintf(l->arg, 10, "%ld",
								 inst->rusage.ru_nivcsw);
			return;
		case LOG_EXITCODE:
			if(WIFEXITED(inst->status)) {
				l->len =
					snprintf(l->arg, 6, "%d",
									 WEXITSTATUS(inst->status));
				return;
			}
			if(WIFSIGNALED(inst->status)) {
				l->len =
					snprintf(l->arg, 6, "SIG%d",
									 WTERMSIG(inst->status));
				return;
			}
			return;										/* just in case */
		case LOG_TIME:
			l->len =
				snprintf(l->arg, 10, "%ld:%02ld:%02ld",
								 (inst->stop - inst->start) / 3600,
								 (inst->stop - inst->start) % 3600 / 60,
								 (inst->stop - inst->start) % 60);
			return;
		case LOG_ATIME:
			tt = htonl(time(NULL) + 2208988800UL);
			
			l->len = sizeof(tt);
			memcpy(l->arg, &tt, sizeof(tt)); /* ugh */
			break;
		case LOG_CTIME:
			tt = time(NULL);
			l->len =
				snprintf(l->arg, 34, "%s", ctime(&tt));
			break;
		default:
			rl_fatal(EX_SOFTWARE, _("Unknown log request"));
	}
}

/* vim: set ts=2: */
