#include "config.h"

#ifdef HAVE_RPC_RPC_H
# include <rpc/rpc.h>
#endif
#ifdef HAVE_RPC_PMAP_CLNT_H
# include <rpc/pmap_clnt.h>
#endif

#include <stdlib.h>
#include "data.h"
#include "error.h"
#include "parse.h"
#include "rlinetd.h"

void rlp_cleanup(struct rl_cleanup *rlc) {
	struct numlist *nl;
	struct rlc_unrpc *rlcu;
	struct rl_cleanup *next;

	do {
		switch(rlc->type) {
			case RLC_UNRPC:
				rlcu = (struct rlc_unrpc *)rlc->data;
				if((nl = rlcu->vers)) {
					do {
						pmap_unset(rlcu->prog, nl->num);
					} while((nl = nl->next));
				}
				numlist_free(rlcu->vers);
				break;
			default:
				rl_warn(_("unknown cleanup type %d"), rlc->type);
		}
		if(rlc->data)
			free(rlc->data);
		next = rlc->next;
		free(rlc);
	} while((rlc = next));
}

/* vim: set ts=2: */
