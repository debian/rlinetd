#ifndef H_DATA
#define H_DATA

#include <sys/time.h>
#include <sys/types.h>
#include <sys/resource.h>

#include "config.h"

#include "bytecode.h"
#include "rlinetd.h"
#include "assemble.h"

#ifdef HAVE_CAPABILITIES
#include <sys/capability.h>
#endif

struct numlist {
	long num;
	struct numlist *next;
};

enum { FLAG_USER = 0x1,
	     FLAG_GROUP = 0x2,
	     FLAG_INITGROUPS = 0x4 };


#define has_flag(x, y)   (((x)->sflags & (y)) == (y))
#define set_flag(x, y)   ((x)->sflags |= (y))
#define reset_flag(x, y) ((x)->sflags &= (~y))



struct service {
	struct stringlist *port;
	struct stringlist *interface;
	char *name;
	int family;
	int socktype;
	int proto;
	const char *protoname;
	int backlog;
	int limit;
	int wait;
	struct rlimit r;
	char *rpcname;
	long rpcnum;
	struct numlist *rpcvers;
#ifdef HAVE_CAPABILITIES
	cap_t caps;
#endif

	rl_opcode_t opfixups [ ofp_max_fixups ];
	int sflags;     /* uid/gid flags */

	int disabled;   /* if parse error in service declaration */
	int internal;   /* if internal service */

	rl_opcode_t run;  /* opstream to hook onto listening socket */
#ifdef HAVE_NET_BPF_H
	void *filter;                 /* LSF program to attach to sockets */
	int filterlen;                /* length of above LSF program */
#endif
};

void numlist_add(struct numlist **, long);
void numlist_copy(struct numlist **, struct numlist *);
void numlist_free(struct numlist *);

struct stringlist {
	char *str;
	struct stringlist *next;
};

void stringlist_add(struct stringlist **, char *);
void stringlist_copy(struct stringlist **, struct stringlist *);
void stringlist_free(struct stringlist *);

struct userdata {
	uid_t uid;
	gid_t gid;
	char *name;
};

void builduserdata(struct userdata **, char *);
void clearuserdata(struct userdata **);
void newuserdata(struct userdata **);
void userdata_copy(struct userdata **, struct userdata *);

#endif /* !H_DATA */

/* vim: set ts=2: */
